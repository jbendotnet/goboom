FROM golang:1.13 AS builder

LABEL maintainer="Toby Jackson"

WORKDIR $GOPATH/src/warmfusion/goboom

COPY go.mod go.mod
COPY go.sum go.sum

RUN go mod download
COPY . .

ARG APP_VERSION=development
ARG APP_BUILD_DATE=notset

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build  -ldflags "-w -s -X main.appVersion=${APP_VERSION} -X main.appBuildDate=${APP_BUILD_DATE} -extldflags '-static'" -o /go/bin/goboom ./cli

FROM scratch

EXPOSE 3000

COPY --from=builder /go/bin/goboom /go/bin/goboom

ENTRYPOINT ["/go/bin/goboom"]
