package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"contrib.go.opencensus.io/exporter/prometheus"
	realprometheus "github.com/prometheus/client_golang/prometheus"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"
	"warmfusion/goboom"
	"warmfusion/goboom/exporters"
)

var (
	address      = flag.String("address", "0.0.0.0:3000", "the http ip/port to listen on")
	url          = flag.String("url", "/beacon", "the http url that the beacon calls will come in on")
	origin       = flag.String("origin", "", "an origin regex to validate beacon requests against. If set an origin header must be present")
	env          = flag.String("env", "local", "Environment indicator for log separation on centralised log systems")
	logFormat    = flag.String("logformat", "text", "Log format (json or text")
	logLevel     = flag.String("loglevel", "debug", "Log level (debug, info, warning, notice)")
	appName      = "goboom"
	appVersion   = "development" // set with -ldflags "-X main.appVersion=1.0.0"
	appBuildDate = "snapshot"    // set with -ldflags "-X main.appBuildDate=$(date +%FT%T%z)"
)

func main() {
	flag.Parse()

	mux := http.NewServeMux()

	logger := goboom.MakeAppLogger(*env, *logFormat, *logLevel, appName, appVersion)

	logger.Infof("Starting GoBoom %s BuildDate: %s", appVersion, appBuildDate)

	pbe := goboom.RegisteredPrometheusExporters{
		Logger: logger,
	}
	pbe.AddExporter(exporters.NavTimingExporter{})

	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	// In our main, register ochttp Server views
	if err := view.Register(ochttp.DefaultServerViews...); err != nil {
		log.Fatalf("Failed to register server views for HTTP metrics: %v", err)
	}

	// Stats exporter: Prometheus
	pe, err := prometheus.NewExporter(prometheus.Options{
		Namespace: "goboom",
		Registry:  realprometheus.DefaultRegisterer.(*realprometheus.Registry),
	})
	if err != nil {
		log.Fatalf("Failed to create the Prometheus stats exporter: %v", err)
	}

	view.RegisterExporter(pe)

	gb := goboom.Handler{
		Exporter: pbe.PrometheusExporter(),
	}

	och := &ochttp.Handler{
		Handler: gb, // The handler you'd have used originally
	}

	if *origin != "" {
		var err error
		gb.Validator, err = goboom.OriginValidator(*origin)
		if err != nil {
			fmt.Printf("Could not compile origin regex: %v\n", err)
			logger.WithError(err).WithField("orgin", origin).Fatal("Unable to set up OriginValidator. Will not continue")
			os.Exit(-1)
			return
		}
		logger.Infof("Origin Validator setup. Only accepting Origin: %s", *origin)

	}

	mux.Handle(*url, och)
	mux.Handle("/metrics", pe)
	mux.Handle("/healthz", http.HandlerFunc(handleHealthz()))
	mux.Handle("/liveness", http.HandlerFunc(handleLiveness()))

	logger.Infof("Listening @ %s for HTTP calls on \"%s\"\n", *address, *url)
	if err := http.ListenAndServe(*address, mux); err != nil {
		fmt.Printf("Could not listen and serve on %s: %v", *address, err)
		os.Exit(1)
	}
}

// handleHealthz - Returns health check for this service indicating that its ok to be used and
//                 should not be restarted
func handleHealthz() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		res.Write([]byte("OK"))
	}
}

// handleHealthz - Returns health check for this service and on failure needs to be restarted
func handleLiveness() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		res.Write([]byte("OK"))
	}
}
