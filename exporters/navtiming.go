package exporters

import (
	"fmt"
	"strconv"
	"warmfusion/goboom"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	performanceTimingTTFB = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_ttfb",
		Help:    "Time to first byte in seconds between starting the http request and getting the first byte of a response",
		Buckets: goboom.WideLatencyBucket,
	}, []string{"hostname"})
	performanceTimingSSLHandshake = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_ssl_handshake",
		Help:    "Duration in seconds to complete the tcp+ssl handshake and establish a TCP connect",
		Buckets: goboom.LowLatencyBucket,
	},
		[]string{"hostname"})
	performanceTimingDNS = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_dns",
		Help:    "Time to complete the DNS lookup",
		Buckets: goboom.LowLatencyBucket,
	},
		[]string{"hostname"})
)

type navTimingMetric struct {
	TTFB         float64
	SSLHandshake float64
	DNSLookup    float64
}

// NavTimingExporter - Creates prometheus data for https://akamai.github.io/boomerang/BOOMR.plugins.NavigationTiming.html
type NavTimingExporter struct {
}

// GetName - Return a friendly name of the exporter for logging
func (nt NavTimingExporter) GetName() string {
	return "NavTiming"
}

// ExportBeaconMetrics - Process the incoming beacon into a prometheus event
func (nt NavTimingExporter) ExportBeaconMetrics(b goboom.Beacon, domain string) error {

	// Use the metrics data to generate a set of values
	// we care about
	bmpt, err := createMetricFromBeacon(b)

	if err != nil {
		return err
	}

	// Now add all the relevant information to prometheus
	performanceTimingTTFB.WithLabelValues(domain).Observe(bmpt.TTFB)
	performanceTimingSSLHandshake.WithLabelValues(domain).Observe(bmpt.SSLHandshake)
	performanceTimingDNS.WithLabelValues(domain).Observe(bmpt.DNSLookup)

	return nil
}

// FindTheDelta - Returns the difference between two keys in the given metrics after checking they're valid
func findTheDelta(startKey string, endKey string, metrics goboom.Metric) (float64, error) {
	st, ok := metrics[startKey]

	if !ok {
		return 0, fmt.Errorf("Cannot find metric %s", startKey)
	}

	end, ok := metrics[endKey]

	if !ok {
		return 0, fmt.Errorf("Cannot find metric %s", endKey)
	}

	start, _ := strconv.Atoi(st)
	endi, _ := strconv.Atoi(end)

	// Assumes ms -> seconds
	return float64(endi-start) / 1000, nil
}

func createMetricFromBeacon(b goboom.Beacon) (navTimingMetric, error) {
	bmpt := navTimingMetric{}

	var err error

	// Parse out metrics into useful struct
	bmpt.TTFB, err = findTheDelta("nt_req_st", "nt_res_end", b.Metrics)

	if err != nil {
		// increment beacon err
		return bmpt, err
	}

	bmpt.SSLHandshake, err = findTheDelta("nt_ssl_st", "nt_con_end", b.Metrics)

	if err != nil {
		// increment beacon err
		return bmpt, err
	}

	bmpt.DNSLookup, err = findTheDelta("nt_dns_st", "nt_dns_end", b.Metrics)

	return bmpt, err
}
