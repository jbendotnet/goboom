package exporters

import (
	"reflect"
	"testing"
	"warmfusion/goboom"
)

type findTheDeltaData struct {
	Name     string
	Start    string
	End      string
	Metric   map[string]string
	Expected float64
	Error    bool
}

func Test_findTheDelta(t *testing.T) {

	cases := []findTheDeltaData{
		findTheDeltaData{
			Name: "Simple test",
			Metric: map[string]string{
				"mystat_st":  "0",
				"mystat_end": "100",
			},
			Start:    "mystat_st",
			End:      "mystat_end",
			Expected: 0.1,
			Error:    false,
		},
		findTheDeltaData{
			Name: "Another test",
			Metric: map[string]string{
				"mystat_st":  "0",
				"mystat_end": "1000000",
			},
			Start:    "mystat_st",
			End:      "mystat_end",
			Expected: 1000,
			Error:    false,
		},
		findTheDeltaData{
			Name:     "Error test",
			Metric:   map[string]string{},
			Start:    "mystat_st",
			End:      "mystat_end",
			Expected: 0,
			Error:    true,
		},
		findTheDeltaData{
			Name: "Missing END metric",
			Metric: map[string]string{
				"mystat_st": "0",
			},
			Start:    "mystat_st",
			End:      "mystat_end",
			Expected: 0,
			Error:    true,
		},
	}

	for _, val := range cases {
		t.Run(val.Name, func(t *testing.T) {
			resp, err := findTheDelta(val.Start, val.End, val.Metric)

			if err != nil && !val.Error {
				t.Errorf("An error occured with %s: %v", val.Name, err)
			}

			if resp != val.Expected {
				t.Errorf("Value does not equal expected in %s: %f, %f", val.Name, resp, val.Expected)
			}
		})
	}
}

func Test_createMetricFromBeacon(t *testing.T) {
	type args struct {
		b goboom.Beacon
	}
	tests := []struct {
		name    string
		args    args
		want    navTimingMetric
		wantErr bool
	}{
		{
			name: "empty Beacon",
			args: args{
				b: goboom.Beacon{},
			},
			want:    navTimingMetric{},
			wantErr: false,
		},
		{
			name: "only TTFB Beacon",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"nt_req_st":  "0",
						"nt_res_end": "100",
					},
				},
			},
			want: navTimingMetric{
				TTFB: 0.1,
			},
			wantErr: false,
		},
		{
			name: "Full navtiming Beacon",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"nt_req_st":  "0",
						"nt_res_end": "100",
						"nt_ssl_st":  "0",
						"nt_con_end": "200",
						"nt_dns_st":  "0",
						"nt_dns_end": "300",
					},
				},
			},
			want: navTimingMetric{
				TTFB:         0.1,
				SSLHandshake: 0.2,
				DNSLookup:    0.3,
			},
			wantErr: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := createMetricFromBeacon(tt.args.b); !reflect.DeepEqual(got, tt.want) {
				if (err != nil) != tt.wantErr {
					t.Errorf("createMetricFromBeacon() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				if got != tt.want {
					t.Errorf("createMetricFromBeacon() = %v, want %v", got, tt.want)
				}

			}
		})
	}
}

func TestNavTimingExporter_GetName(t *testing.T) {
	tests := []struct {
		name string
		nt   NavTimingExporter
		want string
	}{
		{name: "test",
			nt:   NavTimingExporter{},
			want: "NavTiming",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			nt := NavTimingExporter{}
			if got := nt.GetName(); got != tt.want {
				t.Errorf("NavTimingExporter.GetName() = %v, want %v", got, tt.want)
			}
		})
	}
}
