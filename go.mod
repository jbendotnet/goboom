module warmfusion/goboom

go 1.12

require (
	contrib.go.opencensus.io/exporter/prometheus v0.1.0
	github.com/apex/log v1.1.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.3.0
	go.opencensus.io v0.22.2
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
)
