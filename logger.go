package goboom

import (
	"fmt"
	"os"

	"github.com/apex/log"
	logcli "github.com/apex/log/handlers/cli"
	"github.com/apex/log/handlers/discard"
	logjson "github.com/apex/log/handlers/json"
)

type Logger interface {
	WithFields(fields log.Fielder) *log.Entry
	WithField(key string, value interface{}) *log.Entry
	WithError(err error) *log.Entry
	Debug(msg string)
	Info(msg string)
	Warn(msg string)
	Error(msg string)
	Fatal(msg string)
	Debugf(msg string, v ...interface{})
	Infof(msg string, v ...interface{})
	Warnf(msg string, v ...interface{})
	Errorf(msg string, v ...interface{})
	Fatalf(msg string, v ...interface{})
	Trace(msg string) *log.Entry
}

type Config struct {
	Format string
	Level  string
}

func New(cfg Config, fields log.Fields) (*log.Entry, error) {

	switch cfg.Format {
	case "json":
		log.SetHandler(logjson.New(os.Stderr))
	case "text":
		log.SetHandler(logcli.New(os.Stderr))
	default:
		return nil, fmt.Errorf("unsupported log format: %s", cfg.Format)
	}

	switch cfg.Level {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		return nil, fmt.Errorf("unsupported log level: %s", cfg.Level)
	}

	return log.WithFields(fields), nil
}

func MakeAppLogger(env, logFormat, logLevel, appName, appVersion string) *log.Entry {

	cfg := Config{
		Level:  logLevel,
		Format: logFormat,
	}
	fields := log.Fields{
		"env":         env,
		"app.name":    appName,
		"app.version": appVersion,
		"component":   "main",
	}
	logr, _ := New(cfg, fields)
	return logr
}

func MakeTestsLogger() Logger {
	log.SetHandler(discard.Default)
	return log.WithField("tests", true)
}
