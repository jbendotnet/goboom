.PHONY: ci clean dev lint pc run setup test

dev: clean goboom
	go get github.com/smartystreets/goconvey
	goconvey

run: clean goboom
	./goboom \
		-address localhost:3000 \
	  	-origin .* \
		-url "/beacon"

ci: clean setup lint test goboom

pc: clean lint test goboom

clean:
	rm -rf ./goboom

setup:
	go get -t -d -v ./...

lint:
	@echo 'Linting...'
	go fmt
	go vet -all -v

test:
	@echo 'Testing...'
	go test -v -cover ./...

goboom:
	@echo 'building local goboom binary'
	go build -o ./goboom ./cli

docker.build:
	@echo 'building latest'
	docker build --build-arg APP_BUILD_DATE=`date +%FT%T%z` -t warmfusion/goboom:latest .

docker.push:
	@echo 'pushing latest'
	docker push warmfusion/goboom:latest

docker: clean lint test docker.build docker.push
