package goboom

// Useful reference; https://www.w3.org/TR/navigation-timing/

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// The following buckets have been carefully chosen through a series of long
// discussions with a goal of;
//  1. high resolution at areas of interest
//  2. broad enough to handle both cached pages returning quickly, and uncached pages taking longer
var LowLatencyBucket = []float64{0.02, 0.04, 0.06, 0.08, 0.1, 0.15, 0.2, 0.3, 0.5, 1}
var WideLatencyBucket = []float64{0.05, 0.1, 0.15, 0.2, 0.3, 0.5, 1, 2.5, 5, 10}

var (
	beaconError = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "boomerang_collector_beacon_error",
		Help: "Number of errors during collection process",
	},
		[]string{"type"})
)

func getDomainFromBeacon(b Beacon) (string, error) {
	// Extract Domain from b.Source so we have a nice type to include in promehtues
	u, err := url.Parse(b.Source)
	if err != nil {
		return "", fmt.Errorf("Cannot parse url %s", b.Source)
	}

	return u.Hostname(), nil
}

type PrometheusExporter interface {
	ExportBeaconMetrics(Beacon, string) error
	GetName() string
}

type RegisteredPrometheusExporters struct {
	exporters []PrometheusExporter
	Logger    Logger
}

func (t *RegisteredPrometheusExporters) AddExporter(be PrometheusExporter) {
	t.Logger.WithField("exporter", be.GetName()).Info("Adding exporter")
	t.exporters = append(t.exporters, be)
}

func (t RegisteredPrometheusExporters) PrometheusExporter() BeaconExporter {

	return func(req *http.Request, b Beacon) error {

		t.Logger.Info("handling request")
		// Extract site domain from beacon so we can use it
		// as a label to help with aggregation against different
		// sites
		domain, err := getDomainFromBeacon(b)

		if err != nil {
			// Increment beacon err
			beaconError.WithLabelValues("invalid_source").Inc()
			t.Logger.WithError(err).WithField("source", b.Source).Warn("Invalid Source")
			return err
		}

		t.Logger.Debug("Beacon handling begun")
		err = t.handle(b, domain)
		if err != nil {
			beaconError.WithLabelValues("handler").Inc()
			t.Logger.WithError(err).Warn("Handling Failure")
			return err
		}

		t.Logger.Debug("Beacon handling complete")
		// mischief managed...
		return nil
	}

}

func (t *RegisteredPrometheusExporters) handle(bacon Beacon, domain string) error {

	for _, exporter := range t.exporters {
		t.Logger.Debugf("Exporting metrics for %s", exporter.GetName())
		err := exporter.ExportBeaconMetrics(bacon, domain)

		if err != nil {
			beaconError.WithLabelValues(exporter.GetName()).Inc()
			t.Logger.WithError(err).WithField("exporter", exporter.GetName()).Warn("Unable to parse Beacon metrics")
			return err
		}
	}

	return nil
}

// PrometheusExporter(){

// 	x = plugins.each( ){
// 		x.handle(beacon)

// 	}

// }

//---

// PrometheusExporter - Processes incoming request into prometheus events
// which are then presented on /metrics
// func PrometheusExporter() BeaconExporter {

// 	return func(req *http.Request, b Beacon) error {

// 		// Extract site domain from beacon so we can use it
// 		// as a label to help with aggregation against different
// 		// sites
// 		domain, err := getDomainFromBeacon(b)

// 		if err != nil {
// 			// Increment beacon err
// 			beaconError.WithLabelValues("invalid_source").Inc()
// 			return err
// 		}

// 		// mischief managed...
// 		return nil
// 	}
// }
