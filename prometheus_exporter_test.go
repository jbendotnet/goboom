package goboom

import (
	"testing"
)

func Test_getDomainFromBeacon(t *testing.T) {
	type args struct {
		b Beacon
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "validsource",
			args: args{
				b: Beacon{
					Source: "https://www.example.com",
				},
			},
			want:    "www.example.com",
			wantErr: false,
		},
		// {
		// 	name: "missingsource",
		// 	args: args{
		// 		b: Beacon{},
		// 	},
		// 	want:    "",
		// 	wantErr: true,
		// },
		// {
		// 	name: "invalidsource",
		// 	args: args{
		// 		b: Beacon{
		// 			Source: "_not_a_url{}",
		// 		},
		// 	},
		// 	want:    "",
		// 	wantErr: true,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getDomainFromBeacon(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("getDomainFromBeacon() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getDomainFromBeacon() = %v, want %v", got, tt.want)
			}
		})
	}
}

type TestPrometheusExporter struct {
	invokedCount int
}

func (nt *TestPrometheusExporter) ExportBeaconMetrics(b Beacon, domain string) error {
	nt.invokedCount++
	return nil
}
func (nt TestPrometheusExporter) GetName() string { return "TestPrometheusExporter" }

var testExporter = TestPrometheusExporter{}

func TestRegisteredPrometheusExporters_handle(t *testing.T) {
	type fields struct {
		exporters []PrometheusExporter
		Logger    Logger
	}
	type args struct {
		bacon  Beacon
		domain string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantErr  bool
		postFunc func()
	}{
		{
			name:     "No configured exporters",
			fields:   fields{exporters: []PrometheusExporter{}, Logger: MakeTestsLogger()},
			args:     args{bacon: Beacon{}, domain: "example.com"},
			wantErr:  false,
			postFunc: func() {},
		},
		{
			name:    "Simple Test Case",
			fields:  fields{exporters: []PrometheusExporter{&testExporter}, Logger: MakeTestsLogger()},
			args:    args{bacon: Beacon{}, domain: "example.com"},
			wantErr: false,
			postFunc: func() {
				if testExporter.invokedCount == 0 {
					t.Errorf("RegisteredPrometheusExporters.handle() did not invoke test exporter")
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rpe := &RegisteredPrometheusExporters{
				exporters: tt.fields.exporters,
				Logger:    tt.fields.Logger,
			}
			if err := rpe.handle(tt.args.bacon, tt.args.domain); (err != nil) != tt.wantErr {
				t.Errorf("RegisteredPrometheusExporters.handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.postFunc()

		})
	}
}
